#include "pheno_processing.h"
#include <plink2_string.h>
#include <plink2_text.h>
#include <plink_misc.h>

void PhenoInfo::get_pheno_field_information(
    const misc::unordered_str_key_set& is_array_pheno,
    const std::string& pheno_file_name, const char* line_start,
    const char* line_end, bool is_csv, std::vector<PhenoInfo>* pheno_meta,
    std::vector<SQL_table>* pheno_tables,
    misc::unordered_str_key_set* field_in_db, std::size_t* sample_id_idx,
    sqlite3* db)
{
    SQL_table::begin_transaction(db);
    // the input should be the current line
    assert(line_start != line_end);
    std::vector<std::string_view> column_header;
    const char* token_start = line_start;
    const char* token_end;
    const auto delim = is_csv ? ',' : '\t';
    uint32_t token_len;
    while (token_start <= line_end)
    {
        plink_misc::next_token(&token_start, &token_end, line_end, delim);
        token_len = token_end - token_start;
        misc::remove_quote(&token_start, &token_len);
        column_header.emplace_back(token_start, token_len);
        token_start = ++token_end;
    }
    // we should now know how many field there are
    // pheno_meta stores the information of the column. Including its field ID,
    // array ID and instance ID. With this, we can quickly read through the file
    // and simply update the phenotype value (as instance & array are stored as
    // part of the field ID information in the file header)
    pheno_meta->clear();
    pheno_meta->reserve(column_header.size());
    // pheno_table stores the SQL table for each phenotype. We only need to
    // build the table when we encountered the first instace & array of a new
    // field ID
    pheno_tables->clear();
    pheno_tables->reserve(column_header.size());
    // all phenotype have the same table format. As such, we pre-build the two
    // format (with and without array) in the hope that we avoid much of the
    // error & maybe slight speed up?
    const std::vector<sqlColumn> pheno_column = {
        sqlColumn("sample_id")
            .INT()
            .not_null()
            .foreign("participant", "sample_id"),
        sqlColumn("instance").INT().not_null(),
        sqlColumn("pheno").TEXT().not_null()};
    const std::vector<sqlColumn> pheno_array_column = {
        sqlColumn("sample_id")
            .INT()
            .not_null()
            .foreign("participant", "sample_id"),
        sqlColumn("instance").INT().not_null(),
        sqlColumn("pheno").TEXT().not_null(),
        sqlColumn("array").INT().not_null()};
    // we can now iterate the header
    misc::unordered_str_key_map<std::vector<SQL_table>::size_type>
        processed_fields_dict;
    bool processed_field, array_pheno = false, keep_pheno = false;
    std::vector<std::string_view> field_token(4);
    std::string field_id;
    bool is_rg = false;
    for (size_t i = 0; i < column_header.size(); ++i)
    {
        processed_field = false;
        array_pheno = false;
        auto&& field = column_header[i];
        if (field == "f.eid" || field == "eid")
        {
            *sample_id_idx = i;
            keep_pheno = false;
            if (field == "eid") { is_rg = true; }
        }
        else
        {
            // not the sample idx
            // not sample index
            // regeneron's csv file seems to have rather special format where f.
            // is ignored and has format of field-instance-array. We will need
            // to take care of that
            try
            {
                plink_misc::get_field_id_info(field, &field_token, &is_rg);
            }
            catch (...)
            {
                throw std::runtime_error("Error: We expect all Field ID "
                                         "from the phenotype to have either "
                                         "one of the follwoing format "
                                         "following format: f.x.x.x of x-x.x: "
                                         + std::string(field));
            }
            // field_id = field_token[1];
            // instance_str = field_token[2];
            // array_str = field_token[3];
            // check if field is found
            field_id = field_token[1];
            auto found_in_db =
                field_in_db->find(field_id) != field_in_db->end();
            processed_field = processed_fields_dict.find(field_id)
                              != processed_fields_dict.end();
            if (found_in_db && !processed_field)
            {
                fprintf(stderr,
                        "Warning: Duplicated Field ID (%s) detected in %s. "
                        "We will ignore this instance\n",
                        field_token[1].data(), pheno_file_name.c_str());
                keep_pheno = false;
            }
            else
            {
                // only bother to check if phenotype is an array phenotype if we
                // need this field
                array_pheno =
                    (is_array_pheno.find(field_id) != is_array_pheno.end());
                field_in_db->insert(field_id);
                keep_pheno = true;
            }
        }
        // now generate the field tables

        // now generate the field
        if (keep_pheno)
        {
            if (!processed_field)
            {
                // first encounter of this field
                // prefix table name with f as SQL don't allow pure
                // numeric table
                auto table_idx = pheno_tables->size();
                processed_fields_dict[field_id] = table_idx;
                pheno_meta->emplace_back(field_id, field_token[2],
                                         field_token[3], table_idx);

                pheno_tables->emplace_back("f" + field_id, db);
                auto&& cur_table = pheno_tables->back();
                if (array_pheno)
                    cur_table.create_table_and_statement(pheno_array_column,
                                                         false);
                else
                {
                    cur_table.create_table_and_statement(pheno_column, false);
                    pheno_meta->back().not_array();
                }
                field_in_db->insert(field_id);
            }
            else
            {
                // link to previous
                pheno_meta->emplace_back(field_id, field_token[2],
                                         field_token[3],
                                         processed_fields_dict[field_id]);
                if (!array_pheno) pheno_meta->back().not_array();
            }
        }
        else
        {
            // add empty table, which can easily be skiped
            pheno_meta->emplace_back();
        }
    }
    SQL_table::end_transaction(db);
}

/**
 * @brief  add_sample_to_table this function is responsible for checking the
 * sample ID and see if we want to include the sample in the database
 *
 * @param withdrawn A unordered_set containing sample ID of withdrawn samples
 * @param withdrawn_flag A flag to indicate withdrawn. Should be date of program
 * run
 * @param sample_id Current sample ID
 * @param sample_table  The SQL table containing the sample
 * @param add_samples Whether we are adding sample to the db
 * @return true If we want to keep the current sample
 * @return false If we don't want the current sample
 */
bool add_sample_to_table(const misc::unordered_str_key_set& withdrawn,
                         const std::string& withdrawn_flag,
                         const std::string& sample_id, SQL_table* sample_table,
                         bool add_samples)
{
    // withdrawn sample can also be represented by - prefix
    const bool withdrawn_sample =
        sample_id[0] == '-' || withdrawn.find(sample_id) != withdrawn.end();
    // when we are not updating, just remove the samples
    if (add_samples && !withdrawn_sample)
    {
        // add this sample to the SQL table
        // TODO: We might need to use a different query for sample update
        sample_table->run_statement(std::vector<std::string>{
            std::string(sample_id), (withdrawn_sample ? withdrawn_flag : "0")});
    }
    return !withdrawn_sample;
}

void process_pheno_line(const misc::unordered_str_key_set& withdrawn,
                        const std::string& withdrawn_flag,
                        const std::size_t sample_id_idx, const char delim,
                        const char* line_start, const char* line_end,
                        bool add_samples, std::vector<PhenoInfo>* pheno_meta,
                        std::vector<SQL_table>* pheno_tables,
                        SQL_table* sample_table,
                        std::vector<std::string_view>* token_storage,
                        size_t* na_ct, size_t* entry_ct)
{
    // don't use misc::inline split here as the phenotype value can be ill
    // formed. Should instead use the plink_misc::next_token method, which
    // should be more robust
    const char *token_start = line_start, *token_end;
    uint32_t len;
    size_t token_idx = 0;
    const auto max_size = token_storage->size();
    while (token_start < line_end)
    {
        if (unlikely(token_idx >= max_size))
        {
            throw std::runtime_error(
                "Error: Undefined Phenotype file "
                "format! File is expected to have exactly "
                + std::to_string(pheno_meta->size()) + " columns but "
                + std::to_string(token_idx) + " observed!!\n");
        }
        plink_misc::next_token(&token_start, &token_end, line_end, delim);
        len = token_end - token_start;
        misc::remove_quote(&token_start, &len);
        // Assign token to storage
        (*token_storage)[token_idx] = std::string_view(token_start, len);
        token_start = ++token_end;
        ++token_idx;
    }
    // this is also not allowed, as we expect equal length per line
    if (unlikely(token_idx != max_size))
    {
        throw std::runtime_error("Error: Undefined Phenotype file "
                                 "format! File is expected to have exactly "
                                 + std::to_string(pheno_meta->size())
                                 + " columns but " + std::to_string(token_idx)
                                 + " observed!\n");
    }
    // now we have extracted all column, we can process the data
    std::string sample_id = std::string((*token_storage)[sample_id_idx]);
    if (!add_sample_to_table(withdrawn, withdrawn_flag, sample_id, sample_table,
                             add_samples))
    {
        // ignore this line if we don't require this sample
        return;
    }
    // transverse the columns
    for (size_t i = 0; i < token_storage->size(); ++i)
    {
        if (!(*pheno_meta)[i].keep() || i == sample_id_idx) { continue; }
        else if ((*token_storage)[i].length() == 0
                 || (*token_storage)[i] == "NA")
        {
            ++(*na_ct);
            continue;
        }
        // the run_statement function will automatically change the
        // token_storage string_view into string
        (*pheno_meta)[i].run_statement(sample_id, (*token_storage)[i],
                                       pheno_tables);
        ++(entry_ct);
    }
}

void PhenoInfo::build_index(const std::vector<PhenoInfo>& pheno_meta,
                            std::vector<SQL_table>* phenotype_tables)
{
    const std::size_t num_table = phenotype_tables->size();
    std::size_t processed = 0;
    double prev_percentage;
    std::unordered_set<std::vector<SQL_table>::size_type> built_idx;
    bool done;

    for (auto&& pheno : pheno_meta)
    {
        if (!pheno.keep()) continue;
        done = built_idx.find(pheno.table_idx()) != built_idx.end();
        if (done) continue;
        misc::print_progress(processed++, num_table, prev_percentage);
        pheno.build_index(phenotype_tables);
        built_idx.insert(pheno.table_idx());
    }
    misc::print_progress(num_table, num_table, prev_percentage);
}


namespace pheno_processing
{
// we don't really need to know sample_id_idx, as it should almost
// always be the first field, however, no harm taking into account of
// that

/**
 * @brief process_pheno_file The master function responsible for reading and
 * going through each individual phenotype file
 *
 * @param withdrawn An unordered_set containing sample IDs that were withdrawn
 * from the study
 * @param is_array_pheno  An unordered_set containing field ID that are of array
 * format
 * @param pheno_file_name The name of the phenotype file
 * @param withdrawn_flag  The withdrawn flag. Should be the date of the software
 * run
 * @param field_in_db An unordered_set storing the field IDs taht are currently
 * added to the database
 * @param sample_table  The SQL table of the participant information
 * @param db The database object
 * @param is_csv  A boolean indicating if this is the csv
 * @param add_fields A boolean indicating if we are adding new field
 */
void process_pheno_file(const misc::unordered_str_key_set& withdrawn,
                        const misc::unordered_str_key_set& is_array_pheno,
                        const std::string& pheno_file_name,
                        const std::string& withdrawn_flag,
                        misc::unordered_str_key_set* field_in_db,
                        SQL_table* sample_table, sqlite3* db, bool is_csv,
                        bool add_samples)
{
    uintptr_t line_idx = 0;
    plink2::PglErr reterr = plink2::kPglRetSuccess;
    plink2::TextStream pheno_txs;
    PreinitTextStream(&pheno_txs);
    if (plink2::TextStreamOpenEx(pheno_file_name.c_str(), plink2::kMaxLongLine,
                                 0, 1, nullptr, nullptr, &pheno_txs))
    {
        throw std::runtime_error("Error: Cannot open phenotype file: '"
                                 + pheno_file_name + "' to read!");
    }
    const char* line_iter;
    ++line_idx;
    // read in the header
    line_iter = plink2::TextGet(&pheno_txs);
    if (!line_iter)
    {
        if (likely(!plink2::TextStreamErrcode2(&pheno_txs, &reterr)))
        {
            throw std::runtime_error("Error: Empy phenotype file: '"
                                     + pheno_file_name + "'");
        }
        throw std::runtime_error("Error: Failed to read the phenotype file: '"
                                 + pheno_file_name + "'");
    }
    const char* line_end = plink2::AdvToDelim(line_iter, '\n');
    std::vector<PhenoInfo> pheno_meta;
    std::vector<SQL_table> pheno_tables;
    std::size_t sample_id_idx = 0;
    // build the required field tables
    PhenoInfo::get_pheno_field_information(
        is_array_pheno, pheno_file_name, line_iter, line_end, is_csv,
        &pheno_meta, &pheno_tables, field_in_db, &sample_id_idx, db);
    std::cerr << std::endl
              << "============================================================"
              << std::endl;
    std::cerr << "Start processing phenotype file (`" << pheno_file_name
              << "`) with " << pheno_meta.size() << " entries" << std::endl;
    assert(sample_id_idx < pheno_meta.size());
    // now read each line and pass line to function for dedicated add
    // token storage is a temporary storage for us to read in the columns
    std::vector<std::string_view> token_storage(pheno_meta.size());
    line_iter = ++line_end;
    const char delim = is_csv ? ',' : '\t';
    size_t na_ct = 0, entry_ct = 0;
    SQL_table::begin_transaction(db);
    std::cerr << "\r" << line_idx << " line processed"
              << "\r";
    for (; TextGetUnsafe2K(&pheno_txs, &line_iter);
         line_iter = plink2::AdvPastDelim(line_iter, '\n'), ++line_idx)
    {

        if (line_idx < 1000 || !(line_idx % 1000))
        {
            std::cerr << "\r" << line_idx << " lines processed"
                      << "\r";
        }
        line_end = plink2::AdvToDelim(line_iter, '\n');
        // just pass line_iter and line_end to function
        try
        {
            process_pheno_line(withdrawn, withdrawn_flag, sample_id_idx, delim,
                               line_iter, line_end, add_samples, &pheno_meta,
                               &pheno_tables, sample_table, &token_storage,
                               &na_ct, &entry_ct);
        }
        catch (const std::runtime_error& er)
        {
            std::cout << std::endl
                      << er.what() << "\t" << line_idx << "\t"
                      << std::string(line_iter, line_end) << std::endl;
            exit(0);
        }
    }
    if (line_idx > 1)
    { std::cerr << "\r" << line_idx << " lines processed" << std::endl; } else
    {
        std::cerr << "\r" << line_idx << " line processed" << std::endl;
    }
    SQL_table::end_transaction(db);
    SQL_table::begin_transaction(db);
    std::cerr << std::endl << "Start creating indexs" << std::endl;
    PhenoInfo::build_index(pheno_meta, &pheno_tables);
    std::cerr << std::endl
              << "A total of " << entry_ct << " entries entered into database"
              << std::endl;
    std::cerr << "With " << na_ct << " NA entries ignored" << std::endl;
    SQL_table::end_transaction(db);
    plink2::CleanupTextStream(&pheno_txs, &reterr);
}

/**
 * @brief get_existing_table Get the name of all table found in the current
 * database
 *
 * @param db The database object
 * @return misc::unordered_str_key_set An unordered set containing all field IDs
 * currently included in the database
 */
misc::unordered_str_key_set get_existing_table(sqlite3* db)
{
    sqlite3_stmt* stmt;
    sqlite3_prepare(db, "SELECT name FROM sqlite_master WHERE type='table';",
                    -1, &stmt, NULL);
    sqlite3_step(stmt);
    misc::unordered_str_key_set field_id;
    std::string field_id_tmp;
    while (sqlite3_column_text(stmt, 0))
    {
        field_id_tmp = std::string((char*) sqlite3_column_text(stmt, 0));
        if (!field_id_tmp.empty())
        {
            // first and second tables are the code and data showcase?
            field_id_tmp.erase(0, 1);
            field_id.insert(field_id_tmp);
        }
        sqlite3_step(stmt);
    }
    return (field_id);
}

misc::unordered_str_key_set
iterate_through_pheno_files(const misc::unordered_str_key_set& withdrawn,
                            const misc::unordered_str_key_set& is_array_pheno,
                            const std::string& pheno_file_collection,
                            SQL_table* sample_table, bool is_csv, bool addition,
                            sqlite3* db)
{
    std::vector<std::string> file_names;
    misc::split(pheno_file_collection, file_names, ",");
    misc::unordered_str_key_set field_in_db;
    // Add sample indicate if we have the participant table created. If false,
    // then we will contruct the participant table
    bool add_samples = true;
    // generate withdrawn flag as current date. Generate flag here just
    // in case if we processed the phenotype file on different date due
    // to extreme runtime.
    const std::string withdrawn_flag = misc::get_today_date();
    if (addition)
    {
        // get field in db
        field_in_db = get_existing_table(db);
    }
    add_samples = !addition;
    for (auto&& pheno_file : file_names)
    {
        process_pheno_file(withdrawn, is_array_pheno, pheno_file,
                           withdrawn_flag, &field_in_db, sample_table, db,
                           is_csv, add_samples);
        add_samples = false;
    }
    return field_in_db;
}
} // namespace pheno_processing
