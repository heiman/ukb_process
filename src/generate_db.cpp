#include "generate_db.h"
#include <plink2_string.h>
#include <plink2_text.h>
#include <plink_misc.h>
namespace generate_db
{
bool allow_database_construction(const std::string& name, bool addition,
                                 bool allow_replacement)
{
    if (misc::file_exists(name))
    {
        if (addition) return true;
        if (allow_replacement) { misc::remove_file(name); }
        else
        {
            std::cerr << "Error: Database file exists: " + name << std::endl;
            std::cerr << "       Use --replace to replace it" << std::endl;
            return false;
        }
    }
    else if (addition)
    {
        std::cerr << "Error: Database file required for update" << std::endl;
        return false;
    }
    return true;
}

void use_danger(sqlite3* db)
{
    char* zErrMsg = nullptr;
    sqlite3_exec(db, "PRAGMA synchronous = OFF", nullptr, nullptr, &zErrMsg);
    sqlite3_exec(db, "PRAGMA journal_mode = WAL", nullptr, nullptr,
                 &zErrMsg);
}

/**
 * @brief load_code This function is responsible to generate the code reference
 * table
 * @param code_showcase Code showcase file
 * @param db Database object
 */
void load_code(const std::string& code_showcase, sqlite3* db)
{
    std::cerr << std::endl
              << "============================================================"
              << std::endl;
    uintptr_t line_idx = 0;
    plink2::PglErr reterr = plink2::kPglRetSuccess;
    plink2::TextStream pheno_txs;
    PreinitTextStream(&pheno_txs);
    // not going to do multi-thread
    if (plink2::TextStreamOpenEx(code_showcase.c_str(), plink2::kMaxLongLine, 0,
                                 1, nullptr, nullptr, &pheno_txs))
    {
        throw std::runtime_error(
            "Error: Cannot open Code showcase file to read!");
    }
    const char* line_iter;
    ++line_idx;
    // read in the header
    line_iter = plink2::TextGet(&pheno_txs);
    if (!line_iter)
    {
        if (likely(!plink2::TextStreamErrcode2(&pheno_txs, &reterr)))
        { throw std::runtime_error("Error: Empy code showcase file"); }
        throw std::runtime_error(
            "Error: Failed to read the code showcase file");
    }
    line_iter = plink2::AdvPastDelim(line_iter, '\n');

    SQL_table code_table("code", db);
    code_table.create_table_and_statement(std::vector<sqlColumn>{
        sqlColumn("code_id").INT().not_null(),
        sqlColumn("value").INT().not_null(), sqlColumn("meaning").TEXT()});
    SQL_table::begin_transaction(db);
    // code showcase must be csv file
    // and must have format of code, value, meaningm
    // Use string, for some reason, if we use std::string_view instead of
    // std::string, the software slows down rather drastically
    std::vector<std::string> statement_input(3);
    const char *code_start, *token_start, *token_end, *line_end;
    uint32_t code_len;
    std::cerr << "Start loading coding table" << std::endl;
    for (; TextGetUnsafe2K(&pheno_txs, &line_iter);
         line_iter = plink2::AdvPastDelim(line_iter, '\n'), ++line_idx)
    {
        if (line_iter == nullptr) { break; }
        const char* token_iter = line_iter;
        unsigned char ucc = *token_iter;
        while (ucc != ',') { ucc = *(++token_iter); }
        code_start = line_iter;
        code_len = token_iter - line_iter;
        misc::remove_quote(&code_start, &code_len);
        statement_input[0] = std::string(code_start, code_len);
        line_end = plink2::AdvPastDelim(line_iter, '\n');
        // cannot use CsvLexK as there are , in the input
        token_start = plink2::AdvPastDelim(line_iter, ',');
        // need to count the number of " and to take into consideration of
        // escape
        plink_misc::next_csv_token(&token_start, &token_end, line_end);
        code_len = token_end - token_start;
        misc::remove_quote(&token_start, &code_len);
        statement_input[1] = std::string(token_start, code_len);
        token_start = ++token_end;
        plink_misc::next_csv_token(&token_start, &token_end, line_end);
        // minus one here as we are now pointing to after \n
        code_len = token_end - token_start - 1;
        misc::remove_quote(&token_start, &code_len);
        statement_input[2] = std::string(token_start, code_len);
        code_table.run_statement(statement_input);
    }
    SQL_table::end_transaction(db);
    code_table.create_index(std::vector<std::string>{"code_id"});
    plink2::CleanupTextStream(&pheno_txs, &reterr);
    std::cerr << "Completed Coding showcase table load" << std::endl;
}

/**
 * @brief read_withdrawn_samples Function to read in Sample ID that are supposed
 * withdrawn from data
 *
 * @param name Name of file containing the withdrawn samples
 * @return misc::unordered_str_key_set Return an unordered_set containing the
 * sample ID
 */
misc::unordered_str_key_set read_withdrawn_samples(const std::string& name)
{
    bool is_gz = false;
    auto file = misc::load_stream(name, is_gz);
    misc::unordered_str_key_set res;
    std::string line;
    while (std::getline(*file, line))
    {
        misc::trim(line);
        if (line.empty()) continue;
        res.insert(line);
    }
    file.reset();
    return res;
}

/**
 * @brief init_data_meta_table This function will create the table for storing
 * the data showcase
 *
 * @param db the database
 * @param data_meta The data meta object storing the related table
 */
void init_data_meta_table(sqlite3* db, SQL_table* data_meta)
{
    data_meta->create_table_and_statement(std::vector<sqlColumn>{
        sqlColumn("category").INT().not_null(),
        sqlColumn("field_id").INT().primary_key().not_null(),
        sqlColumn("field").TEXT().not_null(),
        sqlColumn("participants").INT().not_null(),
        sqlColumn("items").INT().not_null(),
        sqlColumn("stability").TEXT().not_null(),
        sqlColumn("value_type").TEXT().not_null(), sqlColumn("units").TEXT(),
        sqlColumn("item_type").TEXT(), sqlColumn("strata").TEXT(),
        sqlColumn("sexed").TEXT(), sqlColumn("instances").INT().not_null(),
        sqlColumn("array").INT().not_null(),
        sqlColumn("code_id").INT().foreign("code", "code_id"),
        sqlColumn("included").type("BOOLEAN")});
}

/**
 * @brief load_data Function for loading the data showcase to the data base.
 *
 * @param data_showcase File name of the datashow case information from the uk
 * biobank. Expect to be of CSV format with 17 columns
 * @param addition Indicate if we are just adding new fields. If that's the
 * case, just loop through the data showcase and identify fields that are of
 * array format
 * @param array_field an unordered_set storing Field ID that are of array format
 * @param db The SQL data base object
 */
void load_data(const std::string& data_showcase, const bool addition,
               misc::unordered_str_key_set* array_field, sqlite3* db)
{
    // we will use the plink library here
    misc::unordered_str_key_map<int> column_idx = {
        {"Category", 0},     {"FieldID", 1}, {"Field", 2},
        {"Participants", 3}, {"Items", 4},   {"Stability", 5},
        {"ValueType", 6},    {"Units", 7},   {"ItemType", 8},
        {"Strata", 9},       {"Sexed", 10},  {"Instances", 11},
        {"Array", 12},       {"Coding", 13}};
    std::array<int, 14> file_column_idx;
    file_column_idx.fill(0);
    uintptr_t line_idx = 0;
    plink2::PglErr reterr = plink2::kPglRetSuccess;
    plink2::TextStream pheno_txs;
    PreinitTextStream(&pheno_txs);
    // not going to do multi-thread
    if (plink2::TextStreamOpenEx(data_showcase.c_str(), plink2::kMaxLongLine, 0,
                                 1, nullptr, nullptr, &pheno_txs))
    {
        throw std::runtime_error(
            "Error: Cannot open Data showcase file to read!");
    }
    const char *line_iter, *line_end, *line_start;
    ++line_idx;
    // read in the header
    line_iter = plink2::TextGet(&pheno_txs);
    line_start = line_iter;
    if (!line_iter)
    {
        if (likely(!plink2::TextStreamErrcode2(&pheno_txs, &reterr)))
        { throw std::runtime_error("Error: Empy code showcase file"); }
        throw std::runtime_error(
            "Error: Failed to read the code showcase file");
    }
    // check number
    size_t num_col = 0, max_col_read = 0;
    line_end = plink2::AdvToDelim(line_iter, '\n');
    while (line_iter < line_end)
    {
        const char* token_start = line_iter;
        line_iter = plink2::AdvPastDelim(line_iter, ',');
        // - 1 as we are pointing at the , instead of the end of the token
        uint32_t length = line_iter - token_start - 1;
        misc::remove_quote(&token_start, &length);
        auto&& idx = column_idx.find(std::string(token_start, length));
        if (idx != column_idx.end())
        {
            file_column_idx[idx->second] = num_col;
            if (num_col > max_col_read) { max_col_read = num_col; }
        }
        ++num_col;
    }
    // point to the next line
    line_iter = ++line_end;
    if (num_col != 17)
    {
        throw std::runtime_error(
            "Error: Undefined Data Showcase "
            "format! File is expected to have "
            "exactly 17 columns.\n"
            + std::string(line_start, line_end - line_start));
    }
    std::cerr << std::endl
              << "============================================================"
              << std::endl;
    std::cerr << "Header line of data showcase: " << std::endl;
    std::cerr << std::string(line_start, line_end - line_start) << std::endl;
    // now iterate the file

    SQL_table data_meta("data_meta", db);
    const auto array_idx = file_column_idx[column_idx["Array"]];
    const auto field_idx = file_column_idx[column_idx["FieldID"]];
    if (!addition) { init_data_meta_table(db, &data_meta); }
    else
    {
        // this is an addition, we just re-extract the array information, as we
        // won't do it otherwise
        const char *token_start, *token_end;
        for (; TextGetUnsafe2K(&pheno_txs, &line_iter);
             line_iter = plink2::AdvPastDelim(line_iter, '\n'), ++line_idx)
        {
            line_end = plink2::AdvToDelim(line_iter, '\n');
            line_start = line_iter;
            auto col_read = 0;
            std::string field_id;
            bool is_array = false;
            // now iterate the content until we found all required information
            while (line_iter < line_end
                   && (col_read <= field_idx || col_read <= array_idx))
            {
                token_start = line_iter;
                plink_misc::next_csv_token(&token_start, &token_end, line_end);
                if (col_read == field_idx)
                {
                    uint32_t length = token_end - token_start;
                    misc::remove_quote(&token_start, &length);
                    field_id = std::string(token_start, length);
                }
                else if (col_read == array_idx)
                {
                    uint32_t length = token_end - token_start;
                    misc::remove_quote(&token_start, &length);
                    is_array = (token_start[0] != '1') || length > 1;
                }
                ++col_read;
                line_iter = ++token_end;
            }
            if (is_array) { array_field->insert(field_id); }
        }
        plink2::CleanupTextStream(&pheno_txs, &reterr);
        return;
    }
    SQL_table::begin_transaction(db);
    const char *token_start, *token_end;
    uint32_t length = 0;
    std::vector<std::string> values(14, "");
    std::array<std::string_view, 17> column_value;
    for (; TextGetUnsafe2K(&pheno_txs, &line_iter);
         line_iter = plink2::AdvPastDelim(line_iter, '\n'), ++line_idx)
    {
        line_end = plink2::AdvToDelim(line_iter, '\n');
        line_start = line_iter;
        size_t col_read = 0;
        std::string field_id;
        bool is_array = false;
        // now iterate the content until we found all required information
        while (line_iter < line_end && col_read <= max_col_read)
        {
            token_start = line_iter;
            plink_misc::next_csv_token(&token_start, &token_end, line_end);
            length = token_end - token_start;
            misc::remove_quote(&token_start, &length);
            column_value[col_read] = std::string_view(token_start, length);
            ++col_read;
            line_iter = ++token_end;
        }
        // now we want to assign the value to storage and bind
        for (int i = 0; i < 14; ++i)
        {
            values[i] = std::string(column_value[file_column_idx[i]]);
            if (i == array_idx)
            { is_array = (values[i].length() > 1 || values[i][0] != '1'); } }
        if (is_array) { array_field->insert(std::string(values[field_idx])); }
        data_meta.run_statement(values);
    }
    SQL_table::end_transaction(db);
    data_meta.create_index(std::vector<std::string>{"field_id"});
    plink2::CleanupTextStream(&pheno_txs, &reterr);
    std::cerr << "Complete Data showcase table load" << std::endl;
}

SQL_table construct_participant_table(sqlite3* db)
{
    return SQL_table("participant", db)
        .create_table_and_statement(
            std::vector<sqlColumn>{
                sqlColumn("sample_id").INT().not_null().primary_key(),
                sqlColumn("withdrawn").INT().not_null()},
            false);
}
/**
 * @brief update_data This function will update our data_meta table to indicate
 * if a field is included in the current database. As we did not implement field
 * table removal, we will only add new fields.
 *
 * @param included_fields An unordered_set containing all fields that are
 * included
 * @param db Data base object
 */
void update_data(const misc::unordered_str_key_set& included_fields,
                 sqlite3* db)
{
    // now update
    SQL_table::begin_transaction(db);
    std::string sql =
        "UPDATE data_meta SET included=\"1\" WHERE field_id= @FIELD;";
    sqlite3_stmt* statement = nullptr;
    sqlite3_prepare_v2(db, sql.c_str(), -1, &statement, nullptr);
    int status;
    for (auto&& it : included_fields)
    {
        sqlite3_bind_text(statement, 1, it.c_str(), -1, SQLITE_TRANSIENT);
        status = sqlite3_step(statement);
        if (status != SQLITE_DONE || status == SQLITE_ERROR
            || status == SQLITE_BUSY)
        {
            throw std::runtime_error("Error: Insert failed: "
                                     + std::string(sqlite3_errmsg(db)) + " ("
                                     + std::to_string(status) + ")");
        }
        sqlite3_reset(statement);
    }
    SQL_table::end_transaction(db);
}

void update_sample_table(const misc::unordered_str_key_set& withdrawn_samples,
                         sqlite3* db)
{
    const std::string withdrawn_flag = misc::get_today_date();
    SQL_table::begin_transaction(db);
    std::string sql = "UPDATE participant SET withdrawn=@FIELD WHERE "
                      "sample_id= @ID AND withdrawn == 0;";
    sqlite3_stmt* statement = nullptr;
    sqlite3_prepare_v2(db, sql.c_str(), -1, &statement, nullptr);
    sqlite3_bind_text(statement, 1, withdrawn_flag.c_str(), -1,
                      SQLITE_TRANSIENT);
    int status;
    for (auto&& it : withdrawn_samples)
    {
        sqlite3_bind_text(statement, 2, it.c_str(), -1, SQLITE_TRANSIENT);
        status = sqlite3_step(statement);
        if (status != SQLITE_DONE || status == SQLITE_ERROR
            || status == SQLITE_BUSY)
        {
            throw std::runtime_error("Error: Insert failed: "
                                     + std::string(sqlite3_errmsg(db)) + " ("
                                     + std::to_string(status) + ")");
        }
        sqlite3_reset(statement);
    }
    SQL_table::end_transaction(db);
}

void update_withdrawn_samples(const std::string& withdrawn,
                              const std::string& db_name,
                              const std::string& memory, bool danger)
{
    std::cerr << "Update withdrawn samples" << std::endl;
    if (withdrawn.empty())
    {
        throw std::runtime_error(
            "Error: You must provide the withdrawn sample file to "
            "update the withdrawn status");
    }
    if (misc::file_exists(db_name))
    {
        // TODO: In theory, we don't need to store the sample ID in separated
        // object, can do read load at the same time if needed. (though might
        // encounter problem related to duplicated samples)
        auto drop_out = read_withdrawn_samples(withdrawn);
        if (drop_out.empty())
        { throw std::runtime_error("Error: Withdrawn sample file is empty"); }
        else
        {
            sqlite3* db;
            int rc = sqlite3_open(db_name.c_str(), &db);
            if (rc)
            {
                throw std::runtime_error("Error: Cannot open database: "
                                         + db_name);
            }
            else
            {
                std::cerr << "Opened database: " << db_name << std::endl;
            }
            if (danger) use_danger(db);
            // set_locking_mode(db);
            set_cache_size(memory, db);
            update_sample_table(drop_out, db);
        }
    }
    else
    {
        throw std::runtime_error(
            "Error: You must have a data base to update withdrawn status");
    }
}

bool construct_database(const std::string& name, const std::string& data,
                        const std::string& code, const std::string& memory,
                        const std::string& dropout, const std::string& pheno,
                        const std::string& gp, const std::string& drug,
                        bool is_csv, bool addition, bool danger)
{
    sqlite3* db = nullptr;
    int rc = sqlite3_open(name.c_str(), &db);
    if (rc != SQLITE_OK)
    { throw std::runtime_error("Cannot open database: " + name); }
    std::cerr << "Opened database: " << name << std::endl;
    if (danger) use_danger(db);
    // set_locking_mode(db);
    set_cache_size(memory, db);
    if (!addition) load_code(code, db);
    misc::unordered_str_key_set is_array;
    load_data(data, addition, &is_array, db);
    misc::unordered_str_key_set withdrawn;
    if (!dropout.empty()) { withdrawn = read_withdrawn_samples(dropout); }
    SQL_table sample_table =
        addition ? SQL_table("tmp", db) : construct_participant_table(db);
    auto fields_in_db = pheno_processing::iterate_through_pheno_files(
        withdrawn, is_array, pheno, &sample_table, is_csv, addition, db);
    std::cerr << "A total of " << fields_in_db.size()
              << " fields included in the current database" << std::endl;
    update_data(fields_in_db, db);
    // TODO: Incorporate read code translations to make life easier
    special_tables::load_primary_care_tables(fields_in_db, gp, drug, db);
    sqlite3_close(db);
    return true;
}
} // namespace generate_db
