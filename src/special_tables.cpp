#include "special_tables.h"
namespace special_tables {

void load_gp_record(const std::string &gp_record, sqlite3 *db) {
  bool is_gz = false;
  auto &&gp_file = misc::load_stream(gp_record, is_gz);
  std::string line;
  // there is a header
  auto &&file_length = misc::get_file_length(gp_file, is_gz);
  std::cerr << std::endl
            << "============================================================"
            << std::endl;
  std::cerr << "Header line of primary care record: " << std::endl;
  std::getline(*gp_file, line);
  std::cerr << line << std::endl;
  double prev_percentage = 0;
  SQL_table gp_clinical("gp_clinical", db);
  gp_clinical.create_table_and_statement(std::vector<sqlColumn>{
      sqlColumn("sample_id")
          .INT()
          .not_null()
          .foreign("participant", "sample_id"),
      sqlColumn("data_provider")
          .INT()
          .not_null()
          .foreign("gp_provider", "gp_id"),
      sqlColumn("date_event").TEXT().not_null(), sqlColumn("read2").TEXT(),
      sqlColumn("read3").TEXT(), sqlColumn("value1").TEXT(),
      sqlColumn("value2").TEXT(), sqlColumn("value3").TEXT()});
  SQL_table::begin_transaction(db);
  std::vector<std::string> token, date;
  while (std::getline(*gp_file, line)) {
    // if we trim, then the last line of tab will be problematic
    // e.g. A\tB\t\t\t\t will be problematic
    misc::trim(line);
    if (line.empty())
      continue;
    misc::print_progress(gp_file, file_length, prev_percentage, is_gz);
    // CSV input
    // token = misc::split(line);
    misc::split(line, token, "\t");
    // transform the date format
    try {
      token[2] = transform_date(token[2], date);
    } catch (const std::runtime_error &) {
      continue; // malformed info, skip
    }
    gp_clinical.run_statement(token);
  }
  SQL_table::end_transaction(db);
  fprintf(stderr, "\rProcessing %03.2f%%\n", 100.0);
  gp_clinical.create_index(std::vector<std::string>{"read2", "sample_id"});
  gp_clinical.create_index(std::vector<std::string>{"read3", "sample_id"});
  gp_clinical.create_index(
      std::vector<std::string>{"read3", "read2", "sample_id"});
  gp_clinical.create_index(std::vector<std::string>{"date_event", "sample_id"});
  gp_clinical.create_index(
      std::vector<std::string>{"read3", "read2", "date_event", "sample_id"});
}

void load_prescription(const std::string &drug, sqlite3 *db) {
  bool is_gz;
  auto &&drug_file = misc::load_stream(drug, is_gz);
  std::string line;
  // there is a header
  auto &&file_length = misc::get_file_length(drug_file, is_gz);
  std::cerr << std::endl
            << "============================================================"
            << std::endl;
  std::cerr << "Header line of prescription record: " << std::endl;
  std::getline(*drug_file, line);
  std::cerr << line << std::endl;
  double prev_percentage = 0;
  SQL_table gp_script("gp_scripts", db);
  gp_script.create_table_and_statement(std::vector<sqlColumn>{
      sqlColumn("sample_id")
          .INT()
          .not_null()
          .foreign("participant", "sample_id"),
      sqlColumn("data_provider")
          .INT()
          .not_null()
          .foreign("gp_provider", "gp_id"),
      sqlColumn("date_issue").INT().not_null(),
      sqlColumn("read2").TEXT().not_null(), sqlColumn("bnf_code").TEXT(),
      sqlColumn("dmd_code").TEXT(), sqlColumn("drug_name").TEXT(),
      sqlColumn("quantity").TEXT()});
  SQL_table::begin_transaction(db);
  std::vector<std::string> token, date;
  while (std::getline(*drug_file, line)) {
    misc::trim(line);
    if (line.empty())
      continue;
    misc::print_progress(drug_file, file_length, prev_percentage, is_gz);
    // CSV input
    misc::split(line, token, "\t");
    try {
      token[2] = transform_date(token[2], date);
    } catch (const std::runtime_error &) {
      continue; // malformed info, skip
    }
    gp_script.run_statement(token);
  }
  SQL_table::end_transaction(db);
  fprintf(stderr, "\rProcessing %03.2f%%\n", 100.0);
  gp_script.create_index(std::vector<std::string>{"drug_name", "sample_id"});
  gp_script.create_index(
      std::vector<std::string>{"drug_Name", "date_issue", "sample_id"});
  gp_script.create_index(
      std::vector<std::string>{"drug_name", "data_provider", "sample_id"});
  gp_script.create_index(std::vector<std::string>{
      "drug_Name", "date_issue", "data_provider", "sample_id"});
}

void load_primary_care_tables(const misc::unordered_str_key_set &fields_in_db,
                              const std::string &gp, const std::string &drug,
                              sqlite3 *db) {

  if (gp.empty() && drug.empty()) {
    std::cerr << "No primary care record provided." << std::endl;
    return;
  }
  // remove g, because when we read existing fields, we always remove the
  // first character. It is more efficient to check with a missing character
  // here than going through the whole result later on
  if (fields_in_db.find("p_providers") != fields_in_db.end())
    load_provider(db);
  if (fields_in_db.find("p_clinical") != fields_in_db.end())
    load_gp_record(gp, db);
  else
    std::cerr << "gp_clinical table exists, will not update gp_clinical record"
              << std::endl;
  if (fields_in_db.find("p_scripts") != fields_in_db.end())
    load_prescription(drug, db);
  else
    std::cerr << "gp_scripts table exists, will not update gp_clinical record"
              << std::endl;
}
} // namespace special_tables
