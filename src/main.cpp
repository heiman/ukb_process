#include "commander.h"
#include "generate_db.h"
#include <iostream>

int main(int argc, char *argv[]) {
  Commander commander;
  auto status = commander.parse_command(argc, argv);
  if (status < 1) {
    return status;
  }
  if (commander.update_withdrawn()) {
    generate_db::update_withdrawn_samples(commander.dropout(), commander.out(),
                                          commander.memory(),
                                          commander.danger());
  } else {
    if (!generate_db::allow_database_construction(
            commander.out(), commander.addition(), commander.replace())) {
      return -1;
    }
    generate_db::construct_database(
        commander.out(), commander.data_showcase(), commander.codes(),
        commander.memory(), commander.dropout(), commander.pheno(),
        commander.gp(), commander.drug(), commander.is_csv(),
        commander.addition(), commander.danger());
  }
  std::cerr << std::endl << "Operations completed" << std::endl;
  return 0;
}
