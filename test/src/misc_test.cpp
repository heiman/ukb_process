#include "catch.hpp"
#include "misc.h"
#include <string>
#include <vector>

TEST_CASE("Quote removal")
{
    SECTION("Simple surround")
    {
        std::string exp = "input";
        std::string in = GENERATE("\"input", "input\"", "\"input\"");
        misc::remove_surround_quote(in);
        REQUIRE(in == exp);
    }
    SECTION("Contain sub quotes")
    {
        std::string exp = "input \"something\"";
        std::string in =
            GENERATE("input \"something\"\"", "\"input \"something\"\"");
        misc::remove_surround_quote(in);
        REQUIRE(in == exp);
    }
}
TEST_CASE("CSV read")
{
    std::vector<std::string> result;
    SECTION("Normal csv")
    {
        std::string input = "ID,Website,URL,Description";
        misc::csv_split(input, result);
        std::vector<std::string> expected = {"ID", "Website", "URL",
                                             "Description"};
        REQUIRE(result.size() == expected.size());
        REQUIRE_THAT(result, Catch::Equals(expected));
    }

    SECTION("CSV with quoted comma")
    {
        std::string input = "1,TechTerms,https://techterms.com,\"TechTerms, "
                            "the Computer Dictionary\"";
        misc::csv_split(input, result);
        std::vector<std::string> expected = {
            "1", "TechTerms", "https://techterms.com",
            "TechTerms, the Computer Dictionary"};
        REQUIRE(result.size() == expected.size());
        REQUIRE_THAT(result, Catch::Equals(expected));
    }

    SECTION("CSV with quoted quotes")
    {
        std::string input = "2,Slangit,https://slangit.com,\"Slangit, the "
                            "\"\"Clean Slang\"\" Dictionary\"";
        misc::csv_split(input, result);
        std::vector<std::string> expected = {
            "2", "Slangit", "https://slangit.com",
            "Slangit, the \"\"Clean Slang\"\" Dictionary"};
        REQUIRE(result.size() == expected.size());
        REQUIRE_THAT(result, Catch::Equals(expected));
    }

    SECTION("Complex CSV with quoted quotes")
    {
        // on purposely change size
        result.resize(10);
        std::string input = "2,Slangit,https://slangit.com,\"Slangit, the "
                            "\"\"Clean, Slang\"\", Dictionary\",back";
        misc::csv_split(input, result);
        std::vector<std::string> expected = {
            "2", "Slangit", "https://slangit.com",
            "Slangit, the \"\"Clean, Slang\"\", Dictionary", "back"};
        REQUIRE(result.size() == expected.size());
        REQUIRE_THAT(result, Catch::Equals(expected));
    }

    SECTION("Real data")
    {
        std::string input = "1,\"1\",\"Yes\"";
        misc::csv_split(input, result);
        std::vector<std::string> expected = {"1", "1", "Yes"};
        REQUIRE(result.size() == expected.size());
        REQUIRE_THAT(result, Catch::Equals(expected));
    }
    SECTION("Empty comma")
    {
        std::string input = "a,b,,c,d";
        misc::csv_split(input, result);
        std::vector<std::string> expected = {"a", "b", "", "c", "d"};
        REQUIRE(result.size() == expected.size());
        REQUIRE_THAT(result, Catch::Equals(expected));
    }
    SECTION("Complex empty comma")
    {
        std::string input = ",\"\",\",\",\",,\",";
        misc::csv_split(input, result);
        std::vector<std::string> expected = {"", "", ",", ",,", ""};
        REQUIRE(result.size() == expected.size());
        REQUIRE_THAT(result, Catch::Equals(expected));
    }

    SECTION("Complex inplace empty comma")
    {
        std::string input = ",\"\",\",\",\",,\",";
        std::vector<std::string> expected = {"", "", ",", ",,", ""};
        SECTION("not initialized")
        {
            REQUIRE_THROWS(misc::inplace_csv_split(input, result));
        }
        SECTION("Initialized")
        {
            result.resize(expected.size());
            REQUIRE_NOTHROW(misc::inplace_csv_split(input, result));
            REQUIRE(result.size() == expected.size());
            REQUIRE_THAT(result, Catch::Equals(expected));
        }
    }
}
