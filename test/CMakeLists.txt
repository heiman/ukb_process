cmake_minimum_required(VERSION 3.1 FATAL_ERROR)

add_library(Catch INTERFACE)
set(CATCH_INCLUDE_DIR ${CMAKE_SOURCE_DIR}/test/inc)
target_include_directories(Catch INTERFACE ${CATCH_INCLUDE_DIR})

set(TEST_INCLUDE_DIR ${CMAKE_SOURCE_DIR}/test/inc)
set(TEST_SRC_DIR ${CMAKE_SOURCE_DIR}/test/src)


# Make test executable
set(TEST_SOURCES ${CMAKE_CURRENT_SOURCE_DIR}/catch-main.cpp)
add_executable(tests ${TEST_SOURCES}
    ${CMAKE_CURRENT_SOURCE_DIR}/src/misc_test.cpp
    )
target_include_directories(tests PUBLIC
    ${CMAKE_SOURCE_DIR}/inc)
target_link_libraries(tests PUBLIC
    Catch
    command
    utility)

if(HAS_FS)
    target_link_libraries(tests PRIVATE
        stdc++fs)
endif()
add_test(NAME unitTest COMMAND tests)

add_custom_command(
     TARGET tests
     COMMENT "Run tests"
     POST_BUILD
     COMMAND tests
)

