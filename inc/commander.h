#ifndef COMMANDER_H
#define COMMANDER_H

#include "misc.h"
#include <cstdlib>
#include <getopt.h>
#include <iostream>
#include <sqlite3.h>
#include <stdexcept>
#include <string>
#include <unistd.h>

class Commander {
public:
  Commander();
  int parse_command(int argc, char *argv[]);
  const std::string &out() const { return m_out_name; }
  const std::string &data_showcase() const { return m_data_showcase; }
  const std::string &codes() const { return m_code_showcase; }
  const std::string &dropout() const { return m_drop_name; }
  const std::string &memory() const { return m_memory; }
  const std::string &pheno() const { return m_pheno_name; }
  const std::string &drug() const { return m_drug_name; }
  const std::string &gp() const { return m_gp_name; }
  bool replace() const { return m_replace; }
  bool danger() const { return m_danger; }
  bool addition() const { return m_addition; }
  bool update_withdrawn() const { return m_update_withdrawn; }
  bool is_csv() const { return m_is_csv; }

private:
  inline static const std::string version = "0.4.0";
  inline static const std::string date = "2022-03-28";

  void usage();
  bool valid_inputs();
  std::string m_data_showcase;
  std::string m_code_showcase;
  std::string m_pheno_name;
  std::string m_out_name = "ukb";
  std::string m_memory = "10737418240";
  std::string m_gp_name;
  std::string m_drug_name;
  std::string m_drop_name;
  std::size_t m_threads;
  bool m_danger = false;
  bool m_replace = false;
  bool m_update_withdrawn = false;
  bool m_addition = false;
  bool m_is_csv = false;
};

#endif // COMMANDER_H
