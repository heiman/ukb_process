#ifndef PHENO_PROCESSING_H
#define PHENO_PROCESSING_H

#include "misc.h"
#include <chrono>
#include <ctime>
#include <memory>
#include <sql_table.h>
#include <sqlite3.h>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

class PhenoInfo
{
public:
    PhenoInfo(std::string_view id, std::string_view array, size_t instance,
              std::vector<SQL_table>::size_type table_idx, bool is_array = true)
        : PhenoInfo(id, std::to_string(instance), array, table_idx, is_array)
    {
    }
    PhenoInfo(std::string_view id, std::string_view instance,
              std::string_view array,
              std::vector<SQL_table>::size_type table_idx, bool is_array = true)
        : m_field_id(id), m_table_idx(table_idx), m_is_array(is_array)
    {
        if (is_array)
        {
            statement.resize(4, "");
            statement[1] = std::string(instance);
            statement[3] = std::string(array);
        }
        else
        {
            statement.resize(3, "");
            statement[1] = std::string(instance);
        }
    }
    PhenoInfo(std::string_view id, size_t instance,
              std::vector<SQL_table>::size_type table_idx)
        : PhenoInfo(id, std::to_string(instance), "", table_idx, false)
    {
    }
    PhenoInfo(std::string_view id, std::string_view instance,
              std::vector<SQL_table>::size_type table_idx)
        : PhenoInfo(id, instance, "", table_idx, false)
    {
    }
    PhenoInfo() : PhenoInfo("", "", "", 0, false) { m_keep = false; }
    PhenoInfo& not_array()
    {
        statement.resize(3, "");
        m_is_array = false;
        return *this;
    }

    bool keep() const { return m_keep; }
    std::vector<SQL_table>::size_type table_idx() const { return m_table_idx; }

    void run_statement(const std::string& sample_id, std::string_view pheno,
                       std::vector<SQL_table>* pheno_tables)
    {
        statement[0] = sample_id;
        statement[2] = std::string(pheno);
        (*pheno_tables)[m_table_idx].run_statement(statement);
    }
    static void get_pheno_field_information(
        const misc::unordered_str_key_set& is_array_pheno,
        const std::string& pheno_file_name, const char* line_start,
        const char* line_end, bool is_csv, std::vector<PhenoInfo>* pheno_meta,
        std::vector<SQL_table>* pheno_tables,
        misc::unordered_str_key_set* field_in_db, std::size_t* sample_id_idx,
        sqlite3* db);

    static void build_index(const std::vector<PhenoInfo>& pheno_meta,
                            std::vector<SQL_table>* phenotype_tables);

    void build_index(std::vector<SQL_table>* pheno_tables) const
    {
        if (!m_keep) return;
        auto&& sql = (*pheno_tables)[m_table_idx];
        auto begin = g_idx_query.begin();
        const auto end = begin + g_idx_query.size() - !m_is_array;
        while (begin != end)
        {
            // TODO: Check if we have extract the correct sub vector
            sql.create_index(std::vector<std::string>(begin, end));
            ++begin;
        }
    }

private:
    inline static const std::vector<std::string> g_idx_query = {
        "sample_id", "pheno", "instance", "array"};
    std::vector<std::string> statement;
    const std::string m_field_id;
    const std::vector<SQL_table>::size_type m_table_idx;
    bool m_keep = true;
    bool m_is_array;
};

namespace pheno_processing
{

misc::unordered_str_key_set
iterate_through_pheno_files(const misc::unordered_str_key_set& withdrawn,
                            const misc::unordered_str_key_set& is_array_pheno,
                            const std::string& pheno_file_collection,
                            SQL_table* sample_table, bool is_csv, bool addition,
                            sqlite3* db);
void process_pheno_file(const misc::unordered_str_key_set& withdrawn,
                            const misc::unordered_str_key_set& is_array_pheno,
                            const std::string& pheno_file_name,
                            const std::string& withdrawn_flag,
                            misc::unordered_str_key_set* field_in_db,
                            SQL_table* sample_table, sqlite3* db, bool is_csv,
                            bool add_samples);
} // namespace pheno_processing
#endif // PHENO_PROCESSING_H
