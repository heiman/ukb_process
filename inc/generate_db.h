#ifndef GENERATE_DB_H
#define GENERATE_DB_H

#include "gzstream.h"
#include "misc.h"
#include "pheno_processing.h"
#include "special_tables.h"
#include <iostream>
#include <sql_table.h>
#include <sqlite3.h>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

namespace generate_db
{
/**
 * @brief allow_database_construction   A helper function to simply check if we
 * can construct the db. If db already exists, we will stop unless
 * allow-replacement or addition is true
 * @param name Name of the database file.
 * @param addition  A boolean indicating if we want to add information to
 * existing db
 * @param allow_replacement A boolean indicating if we allow replacing the
 * existing db file
 * @return True if we can proceed, false otherwise
 */
bool allow_database_construction(const std::string& name, bool addition,
                                 bool allow_replacement);
/**
 * @brief construct_database    master function for constructing the required
 * database file.
 * @param name  Name of the final database
 * @param data Name of the data showcase file
 * @param code Name of the code showcase file
 * @param memory    Cache memory to use
 * @param dropout File contain name of the drop out samples
 * @param pheno String containing list of phenotype file. Should be tab
 * separated unless is_csv is true
 * @param gp File containing the gp record.
 * @param drug File containing the drug usage information
 * @param is_csv Indicate if all phenotype files are in csv format
 * @param addition If true, we are adding new fields to existing db
 * @param danger Indicate if we want to use the danger feature that can
 * potentially cause database corruption
 * @return Return true if success, false otherwise.
 */
bool construct_database(const std::string& name, const std::string& data,
                        const std::string& code, const std::string& memory,
                        const std::string& dropout, const std::string& pheno,
                        const std::string& gp, const std::string& drug,
                        bool is_csv, bool addition, bool danger);

/**
 * @brief update_withdrawn_samples  This function is responsible for loading a
 * new withdrrawn list, and add the withdrawn date (the date of running the db
 * update) to the sample list, so that we can know if the sample is withdrawn
 * @param withdrawn File containing the withdrawn samples
 * @param db_name   Database name, if this must exists, or we can't "update" the
 * db
 * @param memory Amount of cache memory to use. Calculated in byte
 * @param danger Indicate we want to use the danger feature, where if server is
 * unstable, this can lead to corrupted db
 */
void update_withdrawn_samples(const std::string& withdrawn,
                              const std::string& db_name,
                              const std::string& memory, bool danger);
/**
 * @brief set_cache_size    Function to set the cache size. Large cache size
 * should in theory speed up the operation
 * @param memory    The amount of cache member in byte
 * @param db The database object.
 */
inline void set_cache_size(const std::string& memory, sqlite3* db)
{
    char* zErrMsg = nullptr;
    sqlite3_exec(db, std::string("PRAGMA cache_size = -" + memory).c_str(),
                 nullptr, nullptr, &zErrMsg);
    sqlite3_exec(db, std::string("PRAGMA mmap_size =" + memory).c_str(),
                 nullptr, nullptr, &zErrMsg);
}

inline void set_locking_mode(sqlite3* db)
{
    char* zErrMsg = nullptr;
    sqlite3_exec(db, std::string("PRAGMA LOCKING_MODE=EXCLUSIVE").c_str(),
                 nullptr, nullptr, &zErrMsg);
}
} // namespace generate_db

#endif // GENERATE_DB_H
