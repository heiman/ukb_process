#ifndef SQL_TABLE_H
#define SQL_TABLE_H

#include <assert.h>
#include <iostream>
#include <sqlite3.h>
#include <string>
#include <vector>

class sqlColumn
{
public:
    sqlColumn(const std::string& name)
    {
        m_name = name;
        m_has_name = true;
    }
    sqlColumn& name(const std::string& name)
    {
        m_name = name;
        m_has_name = true;
        return *this;
    }
    sqlColumn& not_null()
    {
        m_null_string = " NOT NULL";
        return *this;
    }
    sqlColumn& type(const std::string& type)
    {
        m_type = type;
        m_has_type = true;
        return *this;
    }
    sqlColumn& INT() { return type("INT"); }
    sqlColumn& TEXT() { return type("TEXT"); }
    sqlColumn& foreign(const std::string& table, const std::string& id)
    {
        m_foreign_table = table;
        m_foreign_id = id;
        m_is_foreign = true;
        return *this;
    }
    sqlColumn& primary_key()
    {
        m_primary_key = "PRIMARY KEY";
        return *this;
    }
    bool has_foreign() const { return m_is_foreign; }
    std::string get_foreign() const
    {
        assert(has_foreign());
        return "FOREIGN KEY " + bracket(m_name) + " REFERENCES "
               + m_foreign_table + bracket(m_foreign_id);
    }
    std::string bracket(const std::string& input) const
    {
        return "(" + input + ")";
    }
    const std::string get_column_statement() const
    {
        assert(m_has_name && m_has_type);
        return m_name + " " + m_type + " " + m_primary_key + " "
               + m_null_string;
    }
    const std::string& get_name() const { return m_name; }

private:
    std::string m_name = "";
    std::string m_null_string = "";
    std::string m_type = "";
    std::string m_primary_key = "";
    std::string m_foreign_table;
    std::string m_foreign_id;
    bool m_is_foreign = false;
    bool m_has_name = false;
    bool m_has_type = false;
};

class SQL_table
{
public:
    SQL_table(const std::string& name, sqlite3* db)
        : m_table_name(name), m_db(db)
    {
    }
    static void begin_transaction(sqlite3* db)
    {
        char* zErrMsg = nullptr;
        sqlite3_exec(db, "BEGIN TRANSACTION", nullptr, nullptr, &zErrMsg);
    }
    static void end_transaction(sqlite3* db)
    {
        char* zErrMsg = nullptr;
        sqlite3_exec(db, "END TRANSACTION", nullptr, nullptr, &zErrMsg);
    }
    SQL_table& create_table_and_statement(const std::vector<sqlColumn>& columns,
                                          const bool verbose = true);
    void execute_sql(const std::string& sql, bool check_table = false) const
    {
        if (!m_table_constructed && !check_table)
        {
            throw std::runtime_error("Error: Table: " + m_table_name
                                     + " not created");
        }
        char* zErrMsg = nullptr;
        const int rc =
            sqlite3_exec(m_db, sql.c_str(), callback, nullptr, &zErrMsg);
        if (rc != SQLITE_OK)
        {
            sqlite3_free(zErrMsg);
            throw std::runtime_error("SQL error: " + std::string(zErrMsg));
        }
    }
    void create_index(const std::vector<std::string>& fields,
                      const std::string& index_name) const
    {
        std::string sql =
            "CREATE INDEX '" + index_name + "' ON '" + m_table_name + "' (";
        std::string comma = "";
        for (auto&& f : fields)
        {
            sql += comma + "'" + f + "'";
            comma = ",";
        }
        sql += ")";
        // for index, we need a table for that to work
        execute_sql(sql, true);
    }
    void create_index(const std::vector<std::string>& fields) const
    {
        std::string index_name = m_table_name;
        for (auto&& f : fields) { index_name.append("_" + f); }
        index_name.append("_INDEX");
        std::string sql =
            "CREATE INDEX '" + index_name + "' ON '" + m_table_name + "' (";
        std::string comma = "";
        for (auto&& f : fields)
        {
            sql += comma + "'" + f + "'";
            comma = ",";
        }
        sql += ")";
        // for index, we need a table for that to work
        execute_sql(sql, true);
    }
    void run_statement(const std::vector<std::string>& token, const size_t range,
                       const size_t begin = 0)
    {
        bind_statement(token, range, begin);
        process_statement();
    }
    void run_statement(const std::vector<std::string>& token, const size_t begin = 0)
    {
        bind_statement(token, token.size(), begin);
        process_statement();
    }
    const std::string& name() const { return m_table_name; }

private:
    std::string m_table_name;
    sqlite3* m_db = nullptr;
    sqlite3_stmt* m_statement = nullptr;
    bool m_table_constructed = false;
    void create_table(const std::string& sql, const bool verbose);
    void prep_statement(const std::string& sql);
    static int callback(void* /*NotUsed*/, int argc, char** argv,
                        char** azColName)
    {
        int i;
        for (i = 0; i < argc; i++)
        { printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL"); }
        printf("\n");
        return 0;
    }
    void process_statement()
    {
        const int status = sqlite3_step(m_statement);
        if (status != SQLITE_DONE || status == SQLITE_ERROR
            || status == SQLITE_BUSY)
        {
            throw std::runtime_error("Error: Insert failed: "
                                     + std::string(sqlite3_errmsg(m_db)) + " ("
                                     + std::to_string(status) + ")");
        }
        sqlite3_reset(m_statement);
    }
    void bind_statement(const std::vector<std::string>& token, const size_t range,
                        const size_t begin = 0)
    {
        assert(range <= token.size());
        assert(begin < range);
        for (size_t i = begin; i < range; ++i)
        {
            sqlite3_bind_text(m_statement, static_cast<int>(i + 1 - begin),
                              token[i].c_str(), -1, SQLITE_TRANSIENT);
        }
    }
};

#endif // SQL_TABLE_H
